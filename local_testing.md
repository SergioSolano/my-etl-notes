# Docker 

Docker can be used for local testing of:
- Airflow
- Glue Jobs
- Kavak ETL

## Airflow tests
In the DataLakeHouse repo root (you should see docker-compose.yml), run:

    ```
    docker-compose up
    ```
If succesful you should see airflow on 127.0.0.1:8081






## AWS Glue or Kavak ETL tests

### Requirements

AWS SSO credentials must bee specified in ~/.aws/credentials and named [default]

### Setting up the environment for testing

1. Download/update KavakETL repo
2. Build the image

    ```
    docker build -t kavak_etl:dev .
    ```

    This command downloads:
    - AWS Glue image
    - AWS Glue dependencies
    - Kavak ETL dependencies

   Now you should have a local Docker image ready for testing.
   This name should not be changed because is the script will look for it.
   This step should be repeated only when the kavak_etl repo changes.


### Running the script

1. From the DataLakeHouse repo/common/kavak\_etl, pick a local file and send it to docker,
local results will be displayed on the console.


    ```
    ./run_docker_glue.py --app-config-path location/to/app_config.yml
    ```


# Commands taken from history

   44  docker-compose up airflow-init
   45  docker-compose up

  294  docker-compose down

  301  docker-compose stop
  302  docker-compose start

  352  docker ps --format 'table {{.ID}}\t{{.Image}}\t{{.Names}}\t{{.Status}}'


  Server should be working on 127.0.0.1:8081 (default)

