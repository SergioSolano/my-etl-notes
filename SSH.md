# Create ssh key
    ssh-keygen -t ed25519 -C "my_email@mail.com"
    tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy


# EC2 ssh connection
    ssh -i dataEnvKey.pem ec2-user@ec2-34-209-137-7.us-west-2.compute.amazonaws.com
