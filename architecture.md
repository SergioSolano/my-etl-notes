Data Architecture Chapter
------------------------------------------------------------

# martes feb 22 2022

- Data Quality (Ezequiel)
    Agregar información al Handbook.

- Spark en KavakETL (Francisco)
    landing -> refined únicamente

- dim_date (Raúl)
    dim_date se modifica el 3 de marzo. La nueva tabla ya se puede consultar, se
    llama dim_date_2. el 3 de marzo dim_date_2 será dim_date y dim_date se llamará
    dim_date_old. el 18 de marzo desaparece dim_date_old.

- DataDog y Sentry
    Aimeé: no funciona como debería dentro de Glue. En cambio usar Amazon XRay y
    otra herramienta.
    Problema: tanta notificación en Slack se pierde, usar IssueTracker de DataDog
    que permite tomar todos los errores y agruparlos, graficar
    Sentry no es recomendado por Infra, hay que justificar por qué se propone. Sentry
    es un issue-tracker. OpsGenie (Jonathan) cuenta issues, hace escalamiento, llama
    por teléfono, escalamientos por niveles, integración con Jira.

- Cambio de nombre de carpetas
    Los nombres se tienen que cambiar dentro de KavakETL de config -> glue_job
    dentro de la carpeta GlueJob tener sólo lo relacionado a KavakETL. Cosas 
    independientes a KavakETL, tenerlas afuera de la carpeta GlueJob.

- División de datos por países

    Quién ha trabajado con Data Lakes, cómo se maneja la info de diferentes países,
    lo ven todos? Alguien tiene experiencia con esto?
    Depende de regulaciones del país, por ejemplo cuando se quiere eliminar los datos
    en México parece que hay un periodo de tiempo para eliminar la info.

    Problema: el datalake tendrá info de varios países (centralizado), qué riesgos
    hay de tener la info centralizada. Por ahora sólo podemos restringir a nivel
    de columnas, no a nivel de datos. Al dar acceso por zonas, puedes ver los datos
    de esa zona en todos los países, se quiere restringir este acceso.

    Netsuite tiene info de varios países en un mismo lugar, los power users tedrán
    acceso con Athena y podrán ver los datos de todos los países.

    Francisco: si es BI, se puede poner restricción a nivel fila y columna.
        Acceso a datos de DataLake se puede controlar a nivel de carpetas. Parece
        que la restricción por carpetas no aplica.

    Ezequiel: Si se consume por Athena, se puede particionar por país y así puedes
        hacer restricciones.
        Las normas de mantener datos por cierto tiempo, hace que se necesite Delta Lake
        de otro modo no es posible borrar info en un Data Lake.

    Raúl: Las fuentes se pueden dividir por país, excepto Netsuite. Parece que
        Olimpo es complicado de dividir por país. 

    Amazon recomienda particionar por país. Y qué pasa cuando no se puede hacer
    esa partición?

    Milán: Los roles no son la solución considerando las particiones. Se tiene
        que hacer la división por stages que no sean Landing, o raw layer.

    ¿Qué riesgos tiene tener la información de múltiples países juntos?
    Ezequiel: El volumen crece mucho y los procesos se vuelven más pesados.

    Enrique: Separar por esquema.

    Olimpo se va a seperar o será global? Como global por ahora pero no se sabe
    si se queda así o cambia. (Esto es de TECH.) Entonces hay que revisar con ellos
    cómo están planeando separar por país.
        
        br no puede tener sus datos fuera de br

    los cambios quizás deberían ocurrir primero en TECH y después analizarse acá.


# jueves feb 24 2022

- Estandarizar nombres de squads
    de hoy al prox. martes por Slack

- Se debe limpiar el Data Lake y comenzar a tener recursos con metadatos.

    - Glue Jobs (próximo jueves)
        Llenar el doc: 
        https://docs.google.com/spreadsheets/d/1OGyRZX57g1QhdPAzCpO6pQ-XZDZSN0NSvBO-IKmYZvs/edit#gid=0

    - Limpieza de Data Catalog (próximo jueves)
        https://docs.google.com/spreadsheets/d/1FjyDZ7Ji6P3isqdV2ako7EhB5DqBsUNTVFVTHE08YR0/edit#gid=1672465437

    - En KavakETL:
        Debemos tener tags para saber de quién es el recurso (Glue Job).
        Francisco está trabajando en un diccionario de los tags que serán válidos.

        KavakETL no permitirá Cloud Formation, en su lugar se debe usar hay que usar
        la herramienta de CI que está creando Carlos Guevara y Raúl (harán demo).
    
- Cambio en el proceso de deploy (7 de marzo)
    Reglas:
        - No desplegar a prod después de las 5 pm (MX)
        - No hacer deploy antes de un día no laborable de features nuevos nunca

    Proceso:
        - develope feature
        - peer-code-review of feature (SA or Senior Devs)
        - TM (Victor | Soto) should approve the change
        - Merge/Deploy to prod by (SA or Senior Devs)
        
- Job with Spark (Francisco) - Demo entre landing y refined
    KavakETL
    Objetivo: Landing-Refined para vol muy alto o que no se pueda hacer tan fácil
    en SQL.

    Se puede usar Spark o PySpark por que se hace registro de vistas.

    Template Cloud:
        script_location: pyspark/a script de Python (differente a KavakETL)
    Sólo soporta la escritura en una tabla.
    El resultado debe regresar un df.

    Docstrings de NumPy 
    1.5.0 de KavakETL probablemente



# martes mar 1 2022

- llenar sheets sobre dueños de jobs y tablas 
- dim date cambia pronto
- acrónimos de squads
    para nuevos squads, asignarle un nombre propio
    No se llega a una conclusión.
    Los nombres de los productos debe surgir del squad.
    Solution Architects:
        hace una lista de los productos del squad
- onboarding
    Tener un Jupyter Notebook en interactive sessions 
    4 labs para cada etapa
    - landing Sergio
    - refined
    - staging
    - dwh
    Alrededor de KavakETL

- los gluejobs deben ya tener TAGS


# jueves mar 3 2022
Al documento de los jobs agregar tanta metadata como podamos

Acuerdos de Arquitectura

1. Para el próximo martes, completar el documento de Job. Las columnas Source y
   Target deben tener como nivel de detalle: database.schema.table 
   (Supongo que separado por comas)

2. Se ha implementado una ingesta de datos en Datahub https://datahub.prd.de.kavak.io/
   Ahí podemos ver el linaje de los datos. Se recomienda usarlo y señalar
   fortalezas y debilidades de la herramienta.

3. El documento del punto 1 se va a tener que estar actualizando para generar algo
   como lo que muestra Datahub. Para hacerlo de manera automática, deberemos migrar
   los Glue Jobs a KavakETL (herramienta de creación de Jobs) y ahí agregar
   etiquetas. Se recomienda crear un job de prueba con KavakETL para irnos 
   familiarizando.

