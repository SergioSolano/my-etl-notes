# Create a wheel file

## Requirements

    pip install --upgrade pip
    pip install wheel setuptools

## Procedure

1. Make dir with __init__.py file in it
2. At the same level of dir, make setup.py containig:

    from setuptools import setup, find_packages

    setup(
        name = 'example_wheel', 
        version='1.0', 
        packages=find_packages(),
    )

3. Create wheel file
    python setup.py bdist_wheel
