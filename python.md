# virtual environment

Add a path to the python path options:

1. Using a .pth file:

    cd $(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")
    echo /some/library/path > some-library.pth

2. If you want to change the PYTHONPATH used in a virtualenv, you can add
the following lines to your virtualenv's bin/activate file:

    export OLD_PYTHONPATH="$PYTHONPATH"
    export PYTHONPATH="/the/path/you/want"

Add the following line to your bin/postdeactivate script:

    export PYTHONPATH="$OLD_PYTHONPATH"

This way, the new PYTHONPATH will be set each time you use this virtualenv.
And will be set as it was before on deactivate.
