# Athena 
query a substring
    SUBSTR("ga:date", 1, 6)

query from a date field with a format
    date_format(start_date, '%Y-%M')
