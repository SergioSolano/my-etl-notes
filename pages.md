# Chrome pages
Alacritty
    https://github.com/alacritty/alacritty/wiki/Color-schemes

Airflow
    https://towardsdatascience.com/using-apache-airflow-dockeroperator-with-docker-compose-57d0217c8219
    https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html

Cron
    https://crontab.guru/every-5-minutes

Docker
    https://docs.docker.com/engine/reference/commandline/docker/
    https://docs.docker.com/engine/reference/commandline/compose_up/
    https://medium.com/@saschagrunert/demystifying-containers-part-i-kernel-space-2c53d6979504

