# Kavak Python Style Guide

This document gives coding conventions for the Python code in Kavak and is
widely based on the Python's [PEP 8](https://www.python.org/dev/peps/pep-0008/)
with some examples taken from [Google Python Style Guide](#references).

## Foreword
A style guide is about consistency, however, know when to be inconsistent—sometimes
style guide recommendations just aren't applicable.

When in doubt, **use your best judgment**. Look at other examples and decide
what looks best. And don’t hesitate to ask!

Good reasons to ignore a particular guideline:
 
1. When applying the guideline would make the code less readable.
2. To be consistent with surrounding code that also breaks it.
3. Because the code in question predates the introduction of the guideline and
   there is no other reason to be modifying that code.
4. When the code needs to remain compatible with older versions of Python that
   don’t support the feature recommended by the style guide.

## Checklist / Cheat sheet
- Add a comment in line 1 with your squad and name
- Use descriptive variable names
- Use snake_case variables names; use CamelCase names for classe sonly 
- Use [Black formatter](https://github.com/psf/black)
- Inlcude docstrings to your scripts
- Comment only if needed
- Exceptions should be used as little as possible
- Identify code issues before submission to code review (see [Tools](#tools))
- Use naming conventions (variables, classes, jobs, squads)


## Code Layout
We use Black formatter, "a PEP 8 compliant opinionated formatter", while not
perfect, it lays the foundation for a company-wise Python style.

Black should take care of indentation, line length, blank lines, imports sorting,
quotes, and a bunch of other stuff so we end up with uniform codes.

Besides Black a few recommendations are:
- Leave an empty line before a comment that describes a block of code.
- Add descriptions to your scripts, especially if they are Glue Jobs scripts,
    stating what the job does, its inputs and output. Ideally, add an example of
    how to run the job.

## Language Rules
### Imports
- Use import statements for packages and modules only, not for individual classes
or functions, this prevents names collisions. 
- Use from x import y as z if two modules named y are to be imported or if y is an inconveniently long name.
- Use import y as z only when z is a standard abbreviation (e.g., np for numpy).
- Use the full pathname location of the module.

```python
# yes
from common.python.src.utils import retrieve_secrets
import pandas as pd
```

- Avoid using wildcard imports

```python
# no
from pyspark.sql.functions import *

# yes
from pyspark.sql.functions import col, udf, when

```

- Try to be as explicit as possible, in the following example it is uncertain
  what will be imported, the behavior depends on external factors.

```python
# no
import retrieve_secrets
```

### Exceptions
Keep the usage of exceptions to a minimum, they may cause the control flow to be
confusing.

Make use of built-in exception.

### Global variables
Avoid global variables as much as you can.

### True/False Evaluations
Use the implicit false of Python's data types if at all possible.
```python
# yes
if my_list:
    process(my_list)

# no
if len(my_list) > 0:
    process(my_list)
```

## Tools

As developers, we spend most of our time reading (and not writing) code, thus aim
to write code as clear and consistent as possible.

To achieve this there is a wide
set of tools for formating, linting, sorting, etc. Many of them can be managed by
[pre-commit](https://pre-commit.com/hooks), a tool "for identifying simple issues before submission to code review".



Some useful hooks to consider using with pre-commit:
    
- [detect-private-key](https://pre-commit.com/hooks.html): Detects the presence of private keys.
     
- [black](https://github.com/psf/black): Code formatter.
     
- [mypy](http://mypy-lang.org/): static type checker
     
- [pylint](https://pylint.org/): Source-code, bug and quality checker.
     
- [cfn-python-lint](https://mypy.readthedocs.io/en/latest/index.html): Cloud Formation linter.
     
- [check-json](https://pre-commit.com/hooks.html): Check-json - checks json files for parseable syntax.
     
- [end-of-file-fixer](https://pre-commit.com/hooks.html): Ensures that a file is either empty, or ends with one newline.
     
- [trailing-whitespace](https://pre-commit.com/hooks.html): Trims trailing whitespace.
     
- [isort](https://github.com/PyCQA/isort): Sort your imports.
    


## Coding Recommendations
- Keep your code simple. Don't over-sophisticate if it's not necessary.
- Bare in mind that your code is mend to be read (and understood) by anyone.
- Be explicit.

With this in mind, before coding, ask yourself:
- is OOP currently needed?
- is this lambda function too hard to understand?
- am I showing-off with this comprehension dict?
- is this one-liner easy to understand?

## References
- [Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
- [Docstring Conventions](https://www.python.org/dev/peps/pep-0257/)
- [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)


**Authors:**  {{ git_page_authors }}
