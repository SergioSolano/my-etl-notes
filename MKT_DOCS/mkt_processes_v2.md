
# Marketing aka MKT

In the Marketing squad our goal is to provide dashboards with historic and
up-to-date insights about our marketing campaigns.




## Our Workflow

To produce a dashboard, our general workflow has the following steps:

0. Get raw data from a source
1. Save that raw data
2. Cleanup and prepare the data
3. Rearrange the data into Dims and Facts
4. Produce a dashboard

We implement the workflow in our architecture as follows:

| Steps                                     | Architecture Component | Component Name | Process      |
|-------------------------------------------|:----------------------:|:--------------:|:------------:|
| 0. Get raw data from a source             | Heterogeneous sources  |                | AWS Glue Job |
| 1. Save that raw data                     | AWS S3 Bucket          | landing        | AWS Glue Job |
| 2. Cleanup and prepare the data           | AWS Redshift Schema    | stage          | AWS Glue Job |
| 3. Rearrange the data into Dims and Facts | AWS Redshift Schema    | dwh            | AWS Glue Job |
| 4. Produce a dashboard                    | Google Looker          |                |              |

## Our Dashboards

| dashboard           | dwh source tables  |
|---------------------|--------------------|
|                     |                    |
|                     |                    |
|                     |                    |
|                     |                    |


```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Landing"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

# Cheat Sheets
## Git Conventional Commits
- feat
- fix
- ci
- docs
- chore
- tests
- perf
- refactor

