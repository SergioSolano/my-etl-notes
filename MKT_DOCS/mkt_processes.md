
# Marketing aka MKT

In the Marketing squad our goal is to provide dashboards with historic and
up-to-date insights about our marketing campaigns.




## Our Workflow

To produce a dashboard, our general workflow has the following steps:

0. Get raw data from a source
1. Save that raw data
2. Cleanup and prepare the data
3. Rearrange the data into Dims and Facts
4. Produce a dashboard

We implement the workflow in our architecture as follows:

| Steps                                     | Architecture Component | Component Name | Process      |
|-------------------------------------------|:----------------------:|:--------------:|:------------:|
| 0. Get raw data from a source             | Heterogeneous sources  |                | AWS Glue Job |
| 1. Save that raw data                     | AWS S3 Bucket          | landing        | AWS Glue Job |
| 2. Cleanup and prepare the data           | AWS Redshift Schema    | stage          | AWS Glue Job |
| 3. Rearrange the data into Dims and Facts | AWS Redshift Schema    | dwh            | AWS Glue Job |
| 4. Produce a dashboard                    | Google Looker          |                |              |

## Our Dashboards
## Our Processes

### Landing
For Landing we store our data in S3 Buckets and arrange them in databases
and tables in the AWS Glue Data Catalog. Our databases and tables are:

| Database         | Table                       |
|------------------|-----------------------------|
| google_analytics | goals_mx                    |
| google_analytics | group_3                     |
| google_analytics | group_2                     |
| google_analytics | group_1_1                   |
| google_analytics | group_1_3                   |
| google_analytics | user_minute_region_11102021 |
| google_analytics | group_1_2                   |
| google_analytics | ad_consolidation            |
| facebook         | publisher_platform          |
| facebook         | region_report               |
| facebook         | campaign_metadata           |
| facebook         | adset_metadata              |
| facebook         | ad_metadata                 |
| exchange_rates   | rava                        |
| exchange_rates   | banxico                     |
| exchange_rates   | google_finance              |
| exchange_rates   | bcb                         |
| olimpo_postgres  | user_db_user_user           |
| olimpo_mongo     | user_db_ip_information      |
| google_sheets    | audience                    |
| google_sheets    | metrics                     |
| taboola          | taboola                     |
| aircall          | aircall                     |



#### Processes
landing-google-analytics-

|        query_file           |   Athena table              | 
|-----------------------------|-----------------------------|
|            ad_consolidation | ad_consolidation            | 
|        analytics_group_3_mx | group_3                     | 
|                 operational | group_1_1                   | 
|                 operational | group_1_2                   | 
|                 operational | group_1_3                   | 
|                 operational | group_1_4                   | 
|                 operational | goals_mx                    | 
|                 operational | goals_br                    | 
|                 operational | goals_ar                    | 
|                 operational | goals_tr                    | 
|            product_tracking | product_tracking_mx         | 
|            product_tracking | product_tracking_br         | 
|            product_tracking | product_tracking_ar         | 
|            product_tracking | product_tracking_tr         | 
| user_minute_region_11102021 | user_minute_region_11102021 | 


##### Facts

             landing                  |           stg          |                dwh
        
    google_analytics.ad_consolidation -> stg.ad_consolidation -> dim_ad_online       -> fact_campaign_oline
                                                                 dim_campaign_online

### Stage
Our stage tables are stored in Redshift in a schema called **stg**; our tables
are:

| stg table            | landing source tables               |
|----------------------|-------------------------------------|
| ad_consolidation     | ad_consolidation, taboola, facebook |
| stg_mkt_tv_ads       | metrics                             |
| stg_mkt_tv_audience  | audience                            |
| exchange_rate        | rava, banxico, google_finance, bcb  |
| stg_mkt_user_ip_data | user_db_ip_information              |
| stg_mkt_olimpo_user  | user_db_user_user                   |

### DWH
As the stage tables, the dwh tables are also in Redshift in a schema called
**dwh**.

| dwh dim table           | stg source tables                   |
|-------------------------|-------------------------------------|
| dim_exchange_rate       | exchange_rate                       |
| dim_mkt_channel_online  |                                     |
| dim_mkt_ad_offline      | stg_mkt_tv_ads, stg_mkt_tv_audience |
| dim_mkt_event_type      | stg_mkt_events                      |
| dim_mkt_path_property   |                                     |
| dim_mkt_campaign_online |                                     |
| dim_mkt_ad_online       |                                     |
| dim_currency            |                                     |


| dwh fact table              | stg source tables                                        |
|-----------------------------|----------------------------------------------------------|
| fact_mkt_spend_offline      |                                                          |
| fact_mkt_opp_reg_attributed | STG4, STG7, STG8, stg_pur_registers, dim_geography, DIM3 |
| fact_mkt_ad_online          |                                                          |
| fact_mkt_product_tracking   |                                                          |
| fact_mkt_tv_uplift          |                                                          |
| fact_mkt_tv_audience        |                                                          |
| fact_mkt_users_online       |                                                          |


### Dashboards

| dashboard           | dwh source tables  |
|---------------------|--------------------|
|                     |                    |
|                     |                    |
|                     |                    |
|                     |                    |


```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Landing"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

# Cheat Sheets
## Git Conventional Commits
- feat
- fix
- ci
- docs
- chore
- tests
- perf
- refactor

## Secrets

| API           | dwh source tables  |
|---------------------|--------------------|
|                     |                    |
|                     |                    |
|                     |                    |
|                     |                    |
