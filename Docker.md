# Docker

source:
    https://docs.docker.com/get-started/#what-is-a-container
    https://docs.docker.com/engine/reference/commandline/docker/

## Definitions

Container:
    Isolated groups of processes running on a single host, which fulfill a set
    of common features.

    A process on your machine that has been isolated from all other processes
    on the host machine.

    That isolation leverages kernel namespaces and cgroups.


Container Image:
    Provides a custom filesystem to run a container including everything needed
    to run an application, including:
        - all dependencies
        - configuration
        - scripts
        - binaries, etc.
        
        Configuration:
        - environment variables
        - default command to run
        - other metadata

    Image = {app, os, dependencies, configs, ... }


## Docker process
Write Dockerfile >> Build an image >> Ship an image >> Run an image

    The Dockerfile is a list of commands sent to the docker engine
    Build an image: package everything your application needs to run.
    Run an image: execute your application

## Benefits:

    - CI/CD: Consistently test and deploy your code to different environments.
    - Different versions: easily use different version.
    - Roll Forward: If an error is found, no patching is needed, only ship a new image.

--------------------
## Building an image
You need a Dockerfile with the following data:
    FROM base_image:tag(version_of_the_image)
    WORKDIR /directory_to_create_and_use_as_working_directory
    ENV PORT 80                         # environment variables
    COPY package.json /code/package.json
    RUN npm install
    COPY . /code
    CMD ["node", "src/server.js"]       # call node and pass server.js

    docker build --help

    e.g. docker build --tag hello-world .

--------------------
## CLI 
    docker build --tag image_name:tag path   # writes and names an image with the Dockerfile in path

### Reports

    docker images                # Displays the images
    docker ps                    # Displays the running processes
    docker ps -a                 # Displays all the processes

    docker logs container_name   # Fetch the logs of a container
    docker logs --help           # Show help on that docker command

### Creating
    docker run image_name                       # Runs the image commands and creates its process (container)
    docker run -d image_name                    # Runs the image in a detach way
    docker run -p 8080:80 image_name            # Runs and maps the ports of the host and the process
    docker run --name process_name image_name   # Runs the image and names the container
    e.g.
        docker run -p 8080:80 --name process_name -d image_name

    docker start image_name      # Starts one or more stopped containers

### Removing
    docker rm image_name         # totally removes
    docker stop process_name     # stops the process (containers)

    docker rmi image_name        # Remove image

### Push an image into dockerhub
    docker tag source_img target_img    # Binds the target_img with the source_img
    docker push user_name/repository_name 

--------------------
# docker-compose
Sets up the development space to run a set of services. It composes a lot of
files together. To run, it needs a docker-compose.yml file.

    docker-compose up -d        # Executes your docker-compose.yml file and
                                # creates the images, with the current location
                                # added to their name.

    docker-compose down         # stops your application and removes the containers
--------------------
# Other CLI commands
- docker attach [OPTIONS] CONTAINER
    view the container's output or to control it interactively,
    as though the commands were running directly in your terminal.

- docker exec [OPTIONS] CONTAINER COMMAND [ARG...]
    runs a new (executable) command in a running container.
    e.g. docker exec -it conatiner_name bash        # run bash of that container

--------------------
Clean restart of docker
    docker-compose down
    docker rm -f $(docker ps -a -q)
    docker volume rm $(docker volume ls -q)     # remove volumes (persistent storage)
    docker-compose up -d
