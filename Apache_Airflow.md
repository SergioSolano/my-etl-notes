# Airflow
source: 
    https://airflow.apache.org/docs/apache-airflow/stable/concepts/overview.html
    https://airflow.apache.org/docs/apache-airflow/stable/index.html
    https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html

Idea: a better CRON task manager, with DAGS (Directed Acyclic Graphs) to handle some pipeline workflow.

--------------------
# Running Airflow Locally

    # airflow needs a home, ~/airflow is the default,
    # but you can lay foundation somewhere else if you prefer
    # (optional)
    export AIRFLOW_HOME=~/airflow

    AIRFLOW_VERSION=2.1.2
    PYTHON_VERSION="$(python --version | cut -d " " -f 2 | cut -d "." -f 1-2)"
    # For example: 3.6
    CONSTRAINT_URL="https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${PYTHON_VERSION}.txt"
    # For example: https://raw.githubusercontent.com/apache/airflow/constraints-2.1.2/constraints-3.6.txt
    pip install "apache-airflow==${AIRFLOW_VERSION}" --constraint "${CONSTRAINT_URL}"

    # initialize the database
    airflow db init

    airflow users create \
        --username admin \
        --firstname Peter \
        --lastname Parker \
        --role Admin \
        --email spiderman@superhero.org

    # start the web server, default port is 8080
    airflow webserver --port 8080

    # start the scheduler
    # open a new terminal or else run webserver with ``-D`` option to run it as a daemon
    airflow scheduler

    # visit localhost:8080 in the browser and use the admin account you just
    # created to login. Enable the example_bash_operator dag in the home page

--------------------
# Running Airflow in Docker

## Step 0: meet requirements

Requirements:
    - Docker Community Edition (CE)
    - Docker Compose

## Step 1: Setup the docker-compose.yml

    curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.1.2/docker-compose.yaml'

This file contains several service definitions:

    airflow-scheduler - The scheduler monitors all tasks and DAGs, then triggers the task instances once their dependencies are complete.
    airflow-webserver - The webserver available at http://localhost:8080.
    airflow-worker - The worker that executes the tasks given by the scheduler.
    airflow-init - The initialization service.
    flower - The flower app for monitoring the environment. It is available at http://localhost:5555.
    postgres - The database.
    redis - The redis - broker that forwards messages from scheduler to worker.

## Step 2: setup the environment (directories, .env file, database and user creation)
Some directories in the container are mounted, which means that their contents are synchronized between your computer and the container.
    ./dags - you can put your DAG files here.
    ./logs - contains logs from task execution and scheduler.
    ./plugins - you can put your custom plugins here.

Create the necessary files, directories and initialize the database:
    mkdir ./dags ./logs ./plugins
    echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env

Run database migrations and create the first user account:
    docker-compose up airflow-init

## Step 3: running Airflow
Create and start containers:
    docker-compose up
Check that all container have a healthy status.


# Accessing the environment

After starting Airflow, you can interact with it in 3 ways;
    - running CLI commands.
    - via a browser using the web interface.
    - using the REST API.

## CLI commands
    docker-compose run airflow-worker airflow info

    # Run commands easier on Linux or Mac:
    curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.1.2/airflow.sh'
    chmod +x airflow.sh

    Examples:
    ./airflow.sh info
    ./airflow.sh bash
    ./airflow.sh python

# Stop
To stop and delete containers, delete volumes with database data and download images, run:

docker-compose down --volumes --rmi all

# Next 
1. write a dag in the dags folder
2. link its tasks each to a source file in src folder

--------------------

# Concepts
Operator: model of a task. The task is an operator's instance.



Airflow container
    |
    -- DAGS
        |
        -- tasks
