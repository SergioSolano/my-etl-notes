# Max accepted versions
https://docs.aws.amazon.com/glue/latest/dg/release-notes.html
python==3.7
numpy==1.19.5
pandas==1.1.5

spark==3.1.1

## Pandas bugs
https://pandas.pydata.org/docs/whatsnew/v1.2.0.html#bug-fixes
Bug in concat() and DataFrame constructor where input index names are not preserved in some cases (GH13475)
