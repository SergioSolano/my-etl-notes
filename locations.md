
# Code

## Marketing 
    tv_reporting    https://gitlab.com/kavak-it/data-office/data-engineering/kavak-marketing/tv_monitoring
    semrush         https://gitlab.com/kavak-it/data-office/data-engineering/kavak-marketing/semrush
    linkedIn APIs   https://gitlab.com/kavak-it/data-office/data-engineering/kavak-marketing/linkedin
                    https://gitlab.com/kavak-it/data-office/data-engineering/kavak-marketing/linkedin/-/tree/feat/SESP/extract

## Others
    onboarding      https://gitlab.com/kavak-it/data-office/onboarding

# Parquets
    olimpo ip DB    s3://kavak-datalake-serving-layer/growth/analytics/olimpo-user-db-ip-information/
