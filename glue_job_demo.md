
------------------------------------------------------------
# Glue

Serverless data integration service.


## AWS - Serverless

    - automatic scaling,
    - built-in high availability,
    - pay-for-value billing model,
    - no infrastructure to configure or admin.

    **Automatically manages the computing resources**


## Data Integration

Set and combine data by:
    - discovering                       - combining
    - extracting                        - loading
    - cleaning                          - organizing
    - increasing (adding info)          - normalizing

    **Runs code in response to events**


























------------------------------------------------------------
# Glue Job

Process within Glue.
Idea: Event-driven serverless data integration.
Requirements:
    1. **IAM Role**   AWS identity.
    2. **IAM Policy** Permissions associated with an idenity.
    3. **Glue Job**   Job specifications (job name, script file, resources, etc.).


## Make a Glue Job

1. Via online console
    Pros: not as abstract
    Cons: not enough permissions, e.g. you cannot create a IAM Role.

2. Via code
    Pros: more control over the process
    Cons: complicated































------------------------------------------------------------
# Specifying a (Python shell) Glue Job via code

Minimum requirements:
    1. template.yaml
    2. script.py

## template.yaml

    AWSTemplateFormatVersion: '2010-09-09'  # copy paste
    Transform: AWS::Serverless-2016-10-31   # copy paste

    Parameters: ...        # variables

    Resources:
      AWS::IAM::Policy     # S3 access, databases, secrets manager, ...
      AWS::IAM::Role       # identity associated with the policy
      AWS::Glue::Job       # job details, requirements/resources, and script

      AWS::Glue::Trigger   # when to run the job

## script.py
    from resources import *

    def foo():
        extract  something
        tansform something
        load     something

    if __name__ == '__main__':
        foo()




















------------------------------------------------------------
# Where to run the job?

1. AWS Glue Console.
2. Somewhere else.














































------------------------------------------------------------
# How to run the job on the AWS Console?

1. Write job.
2. Add Gitlab CI/CD config. -> Gitlab-job
3. Push to Gitlab.
4. On Gitlab run the Gitlab-job. Gitlab puts the Airflow-job on the AWS Console.


    image: python:3.7   # Python version in Kavak's AWS cloud.

    default:
      before_script:
          - pip install awscli -U           # required
          - pip install aws-sam-cli -U      # required

    stages:                 # enum options
        - build
        - deploy

    variables: ...          # variables to be used in the job

    SAM Deployment:         # Gitlab-job name
        stage: deploy
        when: manual
        script:
          - sam build -t template.yaml
          - sam deploy -t template.yaml
        tags:
            - kavakCiDataOffice      # Kavak's production user





















------------------------------------------------------------
# Conclusion


                    Python script
        
                          |
                          |
                          |     - sam (Infrastructure as Code - IaC)
                          |     - Gitlab CI
                          |
                          |
                          V
        
                    Triggered Glue Job
                

































------------------------------------------------------------
------------------------------------------------------------


https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html
