# James Powell - SQL

query language SQL
ddl: data definition language
dml: data manipulation language

Judicious undestanding of core concepts

How db people think?

select 1
select 1 + 1 as result

select 
    1 + 1 as rv0
    2 ^ 2 as rv1
    3 * 3 as rv2

psql
    select * 
    from (values
        (1, 2)
        , (3, 4)
        , (5, 6)
    ) as t(x, y)

    postgres works with tuples
    you can select multiple rows at once


```psql

    select * 
    into temporary table test
    from (values
        (1, 2)
        , (3, 4)
        , (5, 6)
    ) as t(x, y)
```




```psql
select * 
from unnest(array[1, 2, 3]) as t(x)

```



```psql
select 
      row(123, 'abc')
    , row(456, 'xyz')

```

Get one independent row of 



sql difficult:
    give me the first and last value
    divide the result by 




    select distinct on (adventurer)
    adveturer, monster
    from vanquishes


    select ... limit 5 offset 100

    offset 100 rows fetch next 2 rows only


select
from
group by -> cannot overlap


select a, count(b)
from table
group by a
order by a

where col like 'A%'

where col ~ '\w+'
where col similar to '[A-Za-z]+'        limited options

where filters row level
having filters on grouped level

Functions
------------------------------------------------------------
# case

# coalesce
coalesce(flag, false) first value that happends to be not null

# nullif
nullif(name, 'n/a')


# Q: Do I care about data types?
    pg_typeof(123)                  -> integer
    pg_typeof(123.456)              -> numeric
    pg_typeof('2000-01-01')         -> unknown
    pg_typeof('2000-01-01'::date)

    cast('2000-01-01' as date)      ->


    select setseed(1);
    random()

    pi()::numeric(10,9)

    select 123.456::money           -> fixed size of decimals (cents)

    '2000-01-01 9:00:00.123456'::timestamp
    '2000-01-01 9:00:00.123456'::timestamp(3)
    '2000-01-01 9:00:00.123456'::timestamp with time zone
    '2000-01-01 9:00:00.123456'::timestamp without time zone

    select(... - ... )::interval seconds as delta
    select(... - ... )::interval minute as delta
