
------------------------------------------------------------
# Glue

Serverless data integration service.


## AWS - Serverless

    - automatic scaling,
    - built-in high availability,
    - pay-for-value billing model,
    - no infrastructure to configure or admin.

    **Automatically manages the computing resources**


## Data Integration

Set and combine data by:
    - discovering                       - combining
    - extracting                        - loading
    - cleaning                          - organizing
    - increasing (adding info)          - normalizing

    **Runs code in response to events**


























------------------------------------------------------------
# Glue Job

Process within Glue.
Idea: Event-driven serverless data integration.
Requirements:
    1. **IAM Role**   AWS identity.
    2. **IAM Policy** Permissions associated with an idenity.
    3. **Glue Job**   Job specifications (job name, script file, resources, etc.).


## Make a Glue Job

1. Via online console
    Pros: not as abstract
    Cons: not enough permissions, e.g. you cannot create a IAM Role.

2. Via code
    Pros: more control over the process
    Cons: complicated
































------------------------------------------------------------
# Specifying a (Python shell) Glue Job via code

Minimum requirements:
    1. template.yaml
    2. script.py

## template.yaml

    AWSTemplateFormatVersion: '2010-09-09'  # copy paste
    Transform: AWS::Serverless-2016-10-31   # copy paste

    Parameters: ...        # variables

    Resources:
      AWS::IAM::Policy     # S3 access, databases, secrets manager, ...
      AWS::IAM::Role       # identity associated with the policy
      AWS::Glue::Job       # job details, requirements/resources, and script

## script.py
    from resources import *

    def foo():
        extract  something
        tansform something
        load     something

    if __name__ == '__main__':
        foo()






















------------------------------------------------------------
# Where to run the job?

1. AWS Glue Console.
2. Somewhere else.














































------------------------------------------------------------
# How to run the job on the AWS Console?

1. Write job.
2. Add Gitlab CI/CD config. -> Gitlab-job
3. Push to Gitlab.
4. On Gitlab run the Gitlab-job
    4.1 Gitlab puts the Airflow-job on the Console


    image: python:3.7   # Python version in Kavak's AWS cloud.

    default:
      before_script:
          - pip install awscli -U           # required
          - pip install aws-sam-cli -U      # required

    stages:                 # enum options
        - build
        - deploy

    variables: ...          # variables to be used in the job

    SAM Deployment:         # Gitlab-job name
        stage: deploy
        when: manual
        script:
          - sam build -t template.yaml
          - sam deploy -t template.yaml --capabilities "$CAPABILITIES" --parameter-overrides Stage="$STAGE" --region "$REGION" --stack-name "$TEMPLATE-$VERSION-$STAGE" --s3-bucket "$BUCKET-$STAGE" --s3-prefix "$PREFIX" --no-confirm-changeset --force-upload
        tags:
            - kavakCiDataOffice      # Kavak's production user




















------------------------------------------------------------
# IaC (Infrastructure as Code)
------------------------------------------------------------
------------------------------------------------------------

sam deploy -t template.yaml

    --capabilities          "CAPABILITY_NAMED_IAM"
    --parameter-overrides   Stage="dev"
    --region                "us-west-2"
    --stack-name            "tv-reporting-v1-dev"
    --s3-bucket             "kavak-datalake-dev"
    --s3-prefix             "sam_templates/glue_template"
    --no-confirm-changeset                                  # prompt to confirm it the computed changeset is to be deployed by SAM CLI.
    --force-upload                                          # Override existing files in the S3 bucket.

stack: collection of AWS resources


    Error: Failed to create changeset for the stack: tv-reporting-v1-dev,
    An error occurred (ValidationError) when calling the CreateChangeSet
    operation: Stack:arn:aws:cloudformation:[MASKED]:405458584171:stack/
    tv-reporting-v1-dev/649395b0-0a83-11ec-9019-068bbc56a39f is in
    ROLLBACK_COMPLETE state and can not be updated.

    Error: Failed to create/update the stack: tv-reporting-v1-dev,
    Waiter StackCreateComplete failed: Waiter encountered a terminal failure
    state: For expression "Stacks[].StackStatus" we matched expected path:
    "ROLLBACK_COMPLETE" at least once


1. Role
    Permisos
        lectura de credenciales
        escritura a S3

2. 


# Extra advantages
Glue runs code in response to events & automatically manages the computing resources.

Works with a AWS Glue Data Catalog.

Produces Python or Scala code for data loading, taking care of:
    - dependency resolution
    - job monitoring
    - retry managing

With a "dynamic frame" it handles semistructured data where you don't need a 
known schema.



    ------------------------------------------------------------
    |   semistructured data -> Glue -> structured data (db)    |
    ------------------------------------------------------------


# Source
    https://www.youtube.com/watch?v=z3HeHlWg88M
    https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html

