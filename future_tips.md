AWS
------------------------------------------------------------
aws secretsmanager list-secrets | listar secrets disponibles
https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html | configurar

En python pueden probar a usarlo de la siguiente manera con el wrapper boto3 del kavak core
# pipeliner @ <path_to_directory>/pipeliner-0.4.7-py3-none-any.whl
from pipeliner.auto.models import secrets

secrets.get_access('/prod/api/linkedin/v2/adAnalyticsV2') #Linkedin mkt endpoint secret

SQL
------------------------------------------------------------
select a sample set of rows     select * from table TABLESAMPLE [SYSTEM|BERNOULLI|REPEATABLE];


Python and environ variables
------------------------------------------------------------
    The other thing you might want to look into is using environment variables:

    import os
    import unittest

    class MyTest(unittest.TestCase):
        USERNAME = "jemima"
        PASSWORD = "password"

        def test_logins_or_something(self):
            print('username:', self.USERNAME)
            print('password:', self.PASSWORD)

    if __name__ == "__main__":
        MyTest.USERNAME = os.environ.get('TEST_USERNAME', MyTest.USERNAME)
        MyTest.PASSWORD = os.environ.get('TEST_PASSWORD', MyTest.PASSWORD)
        unittest.main()

    That will let you run with:

    TEST_USERNAME=ausername TEST_PASSWORD=apassword python mytests.py

    And it has the advantage that you're not messing with unittest's own argument parsing. The downside is it won't work quite like that on Windows.

Git
------------------------------------------------------------


## view diff history of function un file
    git log -L :function:file.py
    git log -L :query_api:query_google_analytics.py






My Athena notes
------------------------------------------------------------

## Casts
str to float
    cast("ga:avgpageloadtime" as real)
str to timestamp
    timestamp '2022-02-08 00:00:00'

select 
    "ga:date", 
    sum(cast("ga:avgpageloadtime" as real)) loadtime, 
    sum(cast("ga:newusers" as real)) newusers
from product_tracking_mx
    group by "ga:date"
    having "ga:date" > '2022-02-01'
    order by "ga:date" desc;

select "ga:avgpageloadtime", "ga:newusers"
    from product_tracking_mx
    group by extraction_date
    having extraction_date between timestamp '2022-02-08 00:00:00' and timestamp '2022-02-09 23:59:59'
    order by extraction_date desc;
    







## You need to understand row_numner over partition order by
select "ga:date", country, sum(cast("ga:adcost"as double)), sum(cast("ga:adclicks"as double)), sum(cast("ga:impressions"as double))
 from(
 select
 row_number() over(partition by "ga:date","ga:source","ga:adwordscampaignid",country order by extraction_date desc) as RID
 ,*
 from google_analytics.ad_consolidation
 ) x
 where RID = 1 and country = 'mx'
 group by 1,2
 order by 1 desc






## Redshift validations
select 
	date_start
	,country_from_account_id AS pais
	,COALESCE(sum(spend),0) as gasto
	,COALESCE(sum(clicks),0) AS clicks
	,COALESCE(sum(impressions),0) AS impresiones
 from stg.stg_mkt_ad_consolidation
 where date_start > '2022-01-31'
 	and country_from_account_id = 'MX'
  	and platform_source = 'google'
 group by 1,2
 ORDER BY 1 DESC, 3
 ;
